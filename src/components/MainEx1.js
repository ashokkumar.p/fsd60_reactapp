import React from 'react'

function MainEx1(props) {

  return (
    <div className='main'>
      <h1>Good Morning {props.name} Welcome to {props.place}</h1>
    </div>
  )
}

export default MainEx1

// function nameChange(){
//   let names= ["Ashok","Pavan","Pasha","Harsha"];
//   let index = Math.floor(Math.random() * names.length)
//   return names[index]
// }