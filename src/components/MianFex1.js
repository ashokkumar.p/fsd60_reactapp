import React, { useState } from 'react'


function MianFex1() {
const [emp, setEmp] = useState({
    name:"Ashok",
    age:30,
    salary:90909,
    location:"Hyderabad"
});

const nameChnage = ()=>{
    setEmp({...emp,name:"Kumar",location:'Chennai'})
    
}
  return (
    <div>
      <h1>Good Morning {emp.name} {emp.age} {emp.salary} {emp.location}</h1>
      <button onClick={nameChnage}>ChangeName</button>
    </div>
  )
}

export default MianFex1
