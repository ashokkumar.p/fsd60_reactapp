import React, { Component } from 'react'

export default class MainC_Ex3 extends Component {
   constructor(){
    super()
    this.state = {
        name:"TalentSprint",
        age:21,
        salary:300000
       }
    }
    changeText(){
      this.setState({
          name:"Ashok"
          })
    }
  render() {
    return (
      <div>
        <h1>{this.state.name}</h1>
        <h1>{this.state.age}</h1>
        <h1>{this.state.salary}</h1>
        <button onClick={()=>this.changeText()}>Change Text</button>
      </div>
    )
  }
}

// Assignemnt:
// ==========
// Create a state object with the following fields and display on browser using a table.
// 1. name: string
// 1. age: int
// 1. salary: double
// 1. maritalStatus: boolean