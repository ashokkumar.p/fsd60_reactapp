import React, { useState } from 'react';
import { Card, CardBody, CardTitle, Container, CardSubtitle, CardText, Button, Row, Col } from 'reactstrap';

function CardComponent() {
    const [data, setData] = useState([
        {
            id: 101,
            title: "Watch",
            subtitle: "Discounts",
            discount: '50% discount',
            cardsdiscription: "This is the description of the watch",
            buttontext: "50%",
            imgurl: "https://m.media-amazon.com/images/I/61+r3+JstZL.jpg"
        },
        {
            id: 102,
            title: "Shoes",
            subtitle: "Discounts",
            discount: '50% discount',
            cardsdiscription: "This is the description of the shoes",
            buttontext: "70%",
            imgurl: "https://cdn.pixabay.com/photo/2016/12/09/11/33/smartphone-1894723_960_720.jpg"
        },
        {
            id: 103,
            title: "Shoes",
            subtitle: "Discounts",
            discount: '50% discount',
            cardsdiscription: "This is the description of the shoes",
            buttontext: "70%",
            imgurl: "https://cdn.pixabay.com/photo/2016/12/09/11/33/smartphone-1894723_960_720.jpg"
        },
        {
            id: 104,
            title: "Shoes",
            subtitle: "Discounts",
            discount: '50% discount',
            cardsdiscription: "This is the description of the shoes",
            buttontext: "70%",
            imgurl: "https://cdn.pixabay.com/photo/2016/12/09/11/33/smartphone-1894723_960_720.jpg"
        },{
            id: 105,
            title: "Shoes",
            subtitle: "Discounts",
            discount: '50% discount',
            cardsdiscription: "This is the description of the shoes",
            buttontext: "70%",
            imgurl: "https://cdn.pixabay.com/photo/2016/12/09/11/33/smartphone-1894723_960_720.jpg"
        },
        {
            id: 106,
            title: "Shirt",
            subtitle: "Discounts",
            discount: '50% discount',
            cardsdiscription: "This is the description of the shoes",
            buttontext: "70%",
            imgurl: "https://cdn.pixabay.com/photo/2016/12/09/11/33/smartphone-1894723_960_720.jpg"
        },
        {
            id: 107,
            title: "Sandles",
            subtitle: "Discounts",
            discount: '50% discount',
            cardsdiscription: "This is the description of the shoes",
            buttontext: "70%",
            imgurl: "https://cdn.pixabay.com/photo/2016/12/09/11/33/smartphone-1894723_960_720.jpg"
        },{
            id: 108,
            title: "Mobile",
            subtitle: "Discounts",
            discount: '50% discount',
            cardsdiscription: "This is the description of the shoes",
            buttontext: "70%",
            imgurl: "https://cdn.pixabay.com/photo/2016/12/09/11/33/smartphone-1894723_960_720.jpg"
        },{
            id: 109,
            title: "Shoes",
            subtitle: "Discounts",
            discount: '50% discount',
            cardsdiscription: "This is the description of the shoes",
            buttontext: "70%",
            imgurl: "https://cdn.pixabay.com/photo/2016/12/09/11/33/smartphone-1894723_960_720.jpg"
        }
        
    ]);

    return (
        <Container>
            <Row xs="1" sm="2" md="4">
            {data.map((item) => (
                    <Col>
                        <Card className="my-2 p-2" color="primary" outline>
                            <img alt="Sample" src={item.imgurl} />
                            <CardBody>
                                <CardTitle tag="h5">{item.title}</CardTitle>
                                <CardSubtitle className="mb-2 text-muted" tag="h6">{item.subtitle}</CardSubtitle>
                                <CardText>{item.cardsdiscription}</CardText>
                                <Button>{item.buttontext}</Button>
                            </CardBody>
                        </Card>
                    </Col>
                ))}
            </Row>
        </Container>
    );
}

export default CardComponent;