import React from 'react'

export default function Header() {
   
  return (
    <div className='header'>
      <h1 >MERN Stack Course</h1>
    </div>
  )
}
