import React, { Component } from 'react'

export default class MainC_Ex2 extends Component {
   
  render() {
 
    return (
      <div className='main'>
        <h1>Good Morning {this.props.city}</h1>
      </div>
    )
  }
}
